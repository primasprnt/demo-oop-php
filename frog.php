<?php

require_once ('animal.php');

class Frog extends animal
{
    public $legh=4;
    public $cold_blooded = "false";
    public function jump()
    {
        echo "Hop Hop";
    }
}