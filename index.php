<?php

require('animal.php');
require('frog.php');
require('ape.php');

$shaun = new Animal("shaun");

echo "Nama Hewan :  $shaun->name <br>"; // shaun
echo "Jumlah Kaki : $shaun->legh <br>"; // 2
echo "Berdarah :  $shaun->cold_blooded <br><br>"; //false

$kodok = new Frog("buduk");

echo "Nama hewan : $kodok->name<br>";
echo "jumlah kaki : $kodok->legh<br>";
echo "Berdarah :  $kodok->cold_blooded<br>";
$kodok->jump(); // "hop hop"
echo "<br>";

$ape = new Ape("sungokong");

echo "<br>";
echo "Nama hewan : $ape->name<br>";
echo "jumlah kaki : $ape->legh<br>";
echo "Berdarah :  $kodok->cold_blooded<br>";
$ape->yell(); //"auoo"
